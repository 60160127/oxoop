import java.util.Scanner;

public class Game {
	private static Board board;
	private Player x;
	private Player o;

	public Game() {
		o = new Player('o');
		x = new Player('x');
		board = new Board(x, o);

	}

	void play() {
		showWelcome();
		for (;;) {
			for (;;) {
				showTable();
				showTurn();
				Input();
				if (board.isFinish() == true) {
					showTable();
					showResult();
					showStat();
					showBye();
					break;
				}
			}
			setNewTable();
		}

	}

	private void showTable() {
		char[][] table = board.getTable();
		System.out.println("  1 2 3");
		for (int i = 0; i < table.length; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(" " + table[i][j]);
			}
			System.out.println();
		}

	}

	private void showWelcome() {
		System.out.println("Welcome to OX Game");
	}

	private void showTurn() {
		System.out.println(board.getCurrent().getName() + " Turn...");

	}

	private void Input() {
		Scanner num = new Scanner(System.in);
		System.out.print("Input row,col : ");
		int row = num.nextInt() - 1;
		int col = num.nextInt() - 1;

		if (row >= 0 && row <= 2 && col >= 0 && col <= 2) {
			if (board.table[row][col] == '-') {
				board.setTable(row, col);
				board.switchTurn();
			} else {
				System.out.println("Please Input row,col Again");
			}
		} else {
			System.out.println("Please Input row,col Again");

		}
	}

	void showResult() {
		System.out.println(board.getWinner().getName() + " Win!!");
	}

	void showBye() {
		System.out.println("Bye bye!!");
		System.out.println();
	}

	void setNewTable() {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				board.table[i][j] = '-';
			}
		}
	}

	void showStat() {
		System.out.println("Stat X Win : " + x.getWin() + " Lose : " + x.getLose());
		System.out.println("Stat O Win : " + o.getWin() + " Lose : " + o.getLose());
		System.out.println("Stat Draw : " + x.getDraw());
	}

}
