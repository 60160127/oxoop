public class Board {
	char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	private Player x;
	private Player o;
	private Player winner;
	private Player current;
	private int turnCount;

	public Board(Player x, Player o) {

		this.x = x;
		this.o = o;
		current = x;
		winner = null;
		turnCount = 0;
	}

	public boolean isFinish() {
		if ((table[0][0] == ('x') && table[0][1] == ('x') && table[0][2] == ('x'))
				|| table[1][0] == ('x') && table[1][1] == ('x') & table[1][2] == ('x')
				|| table[2][0] == ('x') && table[2][1] == ('x') & table[2][2] == ('x')
				|| table[0][0] == ('x') && table[1][0] == ('x') & table[2][0] == ('x')
				|| table[0][1] == ('x') && table[1][1] == ('x') & table[2][1] == ('x')
				|| table[0][2] == ('x') && table[1][2] == ('x') & table[2][2] == ('x')
				|| table[0][0] == ('x') && table[1][1] == ('x') & table[2][2] == ('x')
				|| table[0][2] == ('x') && table[1][1] == ('x') & table[2][0] == ('x')) {
			winner = x;
			x.win();
			o.lose();
		
			return true;

		} else {
			if ((table[0][0] == ('o') && table[0][1] == ('o') && table[0][2] == ('o'))
					|| table[1][0] == ('o') && table[1][1] == ('o') & table[1][2] == ('o')
					|| table[2][0] == ('o') && table[2][1] == ('o') & table[2][2] == ('o')
					|| table[0][0] == ('o') && table[1][0] == ('o') & table[2][0] == ('o')
					|| table[0][1] == ('o') && table[1][1] == ('o') & table[2][1] == ('o')
					|| table[0][2] == ('o') && table[1][2] == ('o') & table[2][2] == ('o')
					|| table[0][0] == ('o') && table[1][1] == ('o') & table[2][2] == ('o')
					|| table[0][2] == ('o') && table[1][1] == ('o') & table[2][0] == ('o')) {
				winner = o;
				o.win();
				x.lose();
				return true;

			} else {
				if (turnCount == 9) {
					System.out.println("Game Draw");
					x.draw();
					o.draw();
					return true;
				} else {
					return false;

				}
			}
		}

	}

	public char[][] getTable() {
		return table;
	}

	public Player getCurrent() {
		return current;
	}

	public Player getWinner() {
		return winner;
	}
	
	
	
	public boolean setTable(int row, int col) {
		table[row][col] = current.getName();
		return true;
	}

	public void switchTurn() {
		if (current.getName() == 'x') {
			current = o;
		} else {
			current = x;
		}
	}
}
