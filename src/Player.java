
public class Player {
	private char name;
	private int win, lose, draw;

	Player(char name) {
		this.name = name;
	}

	public char getName() {
		return name;
	}

	public int getWin() {
		return win;
	}

	public int getLose() {
		return lose;
	}

	public int getDraw() {
		return draw;
	}

	public void win() {
		win++;
	}

	public void lose() {
		lose++;
	}

	public void draw() {
		draw++;
	}
}
